#!/bin/sh -e

TOPDIR="$(dirname $0)/.."
source $TOPDIR/init-build-env build

case "$1" in
    rpi3)
	bitbake gnx-image
	;;

    *)
	echo >&2 "Error: unsupported build target"
	exit 1
	;;
esac
